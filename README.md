# ocaml-courses

Gently curated list of computer science courses taught in OCaml. Updates via Pull Requests are actively encouraged. If you are interested in helping maintain this list or approving PRs, please create an issue and you will be added promptly.

This list is sorted by continent, then country code, then university name. 
This data was initially seeded from ocaml.org. The fields were selected to 
minimize how much data needs to be updated over time, with some exceptions for particularly useful fields. When possible, use 
links which do not refer to specific semesters.

| Course Name | Country | University | Homepage or Professor Site (Last Updated) | Notes |
| --- | --- | --- | --- | --- |
| Introduction to Computers and Programming | IN | Indian Institute of Technology, Delhi | [CSL 101](http://www.cse.iitd.ernet.in/~ssen/csl101/details.html) (?) | alongside Pascal, Java |
| Programming in OCAML | AT | University of Innsbruck | [site](http://cl-informatik.uibk.ac.at/teaching/ss06/ocaml/schedule.php) (2006) | |
| Compilation | DK | Aarhus University | [site](https://kursuskatalog.au.dk/en/course/100489/Compilation) (2020) | alongside Java |
| Programming Paradigms | ES | Facultad de Informática - UDC | [site](https://guiadocente.udc.es/guia_docent/index.php?centre=614&ensenyament=614G01&assignatura=614G01014&fitxa_apartat=3&any_academic=2020_21&idioma=cast&idioma_assig=cast&any_academic=2020_21) (?) | |
| Functional Programming | FR | Aix-Marseille University | ? | |
| Introduction to Algorithms | FR | EPITA | ? | |
| Functional Programming and Introduction to Type Systems | FR | ISAE/Supaéro | ? | |
| Types and static analysis | FR | University Pierre & Marie Curie | [site](https://www-apr.lip6.fr/~chaillou/Public/enseignement/2014-2015/tas/) (2015) | |
| Advanced Functional Programming | FR | Université Paris-Diderot | [PFAV](https://www.dicosmo.org/CourseNotes/pfav/) (2014) | |
| Functional Programming | FR | Université Paris-Diderot | [PF5](https://www.irif.fr/~treinen/teaching/pf5/) (2015) | |
| Models of programming and languages interoperability | FR | University Pierre & Marie Curie | [LI 332](http://www-licence.ufr-info-p6.jussieu.fr/lmd/licence/2014/ue/LI332-2014oct/) (2014) | |
| Compilation | FR | University of Rennes 1 | ? | |
| Semantics | FR | University of Rennes 1 | ? | |
| Programming 2 | FR | University of Rennes 1 | ? | |
| Principals of Programming Languages | IT | CNAM | [site](http://compass2.di.unipi.it/didattica/wif18/share/corsi/corso.asp?id=3515&cds=wif18&anno=2015) (?) | |
| ? | IT | Università Degli Studi Roma Tre - Dipartimento di Ingegneria | ? | |
| Programming | IT | Università Degli Studi di Verona - Dipartimento di Informatica | [site](http://profs.scienze.univr.it/~giaco/styled-16/index.html) (2016) | |
| Functional Programming | PL | University of Wroclaw | [site](http://www.ii.uni.wroc.pl/~lukstafi/pmwiki/index.php?n=Functional.Functional) (2015) | |
| Theory of Computation | PT | University of Beira Interior | [site](https://www.di.ubi.pt/~desousa/TC/) (?) | |
| Formal methods in Programming Languages | RO | University Babeș Bolyai of Cluj-Napoca | ? | |
| Introduction to Computer Science | SG | Yale-NUS College | ? | |
| Foundations of Computer Science | UK | University of Birmingham | [FOCS 1112](https://sites.google.com/site/focs1112/) (?) | |
| Advanced Functional Programming | UK | University of Cambridge | [L 28](https://www.cl.cam.ac.uk/teaching/1415/L28/) (2015) | |
| Programming Languages and Paradigms | CA | McGill University | COMP 302 (?) | |
| Programming Langauges | CA | Université Laval | ? | |
| Computer Science I | US | Boston College | [CS 1101](http://www.cs.bc.edu/~muller/teaching/cs1101/s16/) (2016) | |
| Introduction to Computation | US | Brown University | [CS 17/CS 18](https://cs.brown.edu/courses/cs017/) (2020) | |
| Fundamentals of Computer Programming | US | Caltech | [CS 4](http://users.cms.caltech.edu/~mvanier/) (2017) | |
| Programming Languages and Translators | US | Columbia University | [COMS W4115](http://www1.cs.columbia.edu/~sedwards/classes/2014/w4115-fall/) (2014) | |
| Data Structures and Functional Programming | US | Cornell University | [CS 3110](https://www.cs.cornell.edu/courses/cs3110/) (2020) | |
| Principles of Programming Language Compilation | US | Harvard University | [CS 153](https://www.seas.harvard.edu/courses/cs153/) (2019) | |
| Introduction to Computer Science II: Abstraction & Design | US | Harvard University | CS 51 (?) | |
| Functional Programming | US | Princeton University | [COS 326](https://www.cs.princeton.edu/courses/archive/fall14/cos326/) (2014) | |
| Principles of Programming Languages | US | Rice University | [COMP 311](https://www.cs.rice.edu/~javaplt/311/info.html) (2009) |  |
| Programming Languages | US | University of California, Los Angeles | [CS 131](https://web.cs.ucla.edu/classes/winter18/cs131/) (2018) | alongside Python, Java |
| Programming Languages: Principles and Paradigms | US | University of California, San Diego | [CSE 130-a](https://cseweb.ucsd.edu/classes/wi14/cse130-a/) (2014) | alongside Python, Prolog |
| Programming Languages and Compilers | US | University of Illinois at Urbana-Champaign | [CS 421](https://courses.engr.illinois.edu/cs421/fa2014/) (2014) | |
| Organization of Programming Languages | US | University of Maryland | [CMSC 330](https://www.cs.umd.edu/class/fall2014/cmsc330/) (2014) | alongside Ruby, Prolog, Java |
| Programming Languages | US | University of Massachusetts Amherst | [CMPSCI 631](https://people.cs.umass.edu/~arjun/main/teaching/631/) (2018) | |
| Programming Languages | US | University of Massachusetts Boston | [CS 691F](https://people.cs.umass.edu/~arjun/courses/cs691f/) (?) | |
| Advanced Programming Principles | US | University of Minnesota Twin Cities | [CSCI 2041](https://www-users.cs.umn.edu/~kauffman/2041/syllabus.html) (2018) | |
| Compilers | US | University of Pennsylvania | [CIS 341](https://www.cis.upenn.edu/~cis341/) (2020) | |
| Programming Languages and Techniques I | US | University of Pennsylvania | [CIS 120](https://www.seas.upenn.edu/~cis120/) (2020) | |
| Programming Languages | US | University of Virginia | CS 4610 (?) | |

